<?php
if (isset($_POST["word1"]) && isset($_POST["word2"])) {
	extract($_POST);
	$ld = levenshtein(strtolower($word1), strtolower($word2));
	$longest = (strlen($word1) > strlen($word2) ? strlen($word1) : strlen($word2));
	$pct = round($ld / $longest * 100, 1);
	echo "<p>Case insensitive Levenshtein Distance between $word1 and $word2 is $ld, which is $pct% of the longest string.</p>";
}
?>

<h1>Levenshtein Distance calculator</h1>

<p>Put two words in to see how far apart they are according to Levenshtein Distance.</p>
<form action="leven-test.php" method="post">
	<label>First word:
		<input type="text" name="word1">
	</label><br />
	<label>Second word:
		<input type="text" name="word2">
	</label><br /><br />
	<input type="submit" name="submit" value="Compare">
</form>