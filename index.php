<?php

// helper functions --------------------------

// init date / time vars
$timeZone = new DateTimeZone("America/New_York");
$outputFormat = "D m-d-Y, H:i:s";
$spellCheckerWarningPct = 20;

function formatDate($dateText) {
	global $timeZone;
	global $outputFormat;
	$dateTextFormatted = new DateTime($dateText, $timeZone);
	return $dateTextFormatted->format($outputFormat);
}

function getLength($start, $end) {
	global $timeZone;
	$start = new DateTime($start, $timeZone);
	$end = new DateTime($end, $timeZone);

	$interval = $start->diff($end);

	$days = $interval->format("d");
	$hours = $interval->format("%h");
	$minutes = $interval->format("%i");

	$totalMinutes = ((($days * 24) + $hours) * 60) + $minutes;

	return $totalMinutes;
}

function percentDifference($s1, $s2) {
	$ld = levenshtein(strtolower($s1), strtolower($s2));
	$longest = (strlen($s1) > strlen($s2) ? strlen($s1) : strlen($s2));
	$pct = round($ld / $longest * 100, 1);

	// echo "<p>$s1 vs $s2 returns $pct% difference</p>";

	global $spellCheckerWarningPct;

	if ($pct <= $spellCheckerWarningPct && in_array("spelling", $_POST["expand"])) {
		echo "<p>Spell Checker log: $s1 vs $s2 returns $pct% difference</p>";
	}
	return $pct; // 0-100, 1 decimal place
}

function roomsBetweenLength($lower, $upper = 999999) {
	global $roomLengthData;
	global $totalRooms;

	$total = 0;
	for ($x=$lower; $x<=$upper; $x++) {
		if (array_key_exists($x, $roomLengthData)) {
			$total += $roomLengthData[$x];
		}
	}
	$percent = round($total / $totalRooms * 100, 1);
	if ($upper != 999999) {
		return "<p>$total Rooms with length between $lower and $upper minutes ($percent%).</p>";
	} else {
		return "<p>$total Rooms with length above $lower minutes ($percent%).</p>";
	}
}
?>

<html>
	<head>
		<title>Collaborate CSV Cruncher</title>
		<script language="javascript" type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
	</head>
	<body>

<?php

if (isset($_POST['submit'])) {

// settings
set_time_limit(180);

// broad, shallow check for proper CSV format; reject all others.
$csvMimetypes = array(
	'text/csv',
	'text/plain',
	'application/csv',
	'text/comma-separated-values',
	'application/excel',
	'application/vnd.ms-excel',
	'application/vnd.msexcel',
	'text/anytext',
	'application/octet-stream',
	'application/txt',
);

if (!in_array($_FILES["file"]["type"], $csvMimetypes)) {
	die("<p>Error: you need to submit a valid CSV file.</p>");
}

// generate new file name
$temp = explode(".csv",$_FILES["file"]["name"]);
$newfilename = rand(1,99999) . '.csv';

$path = "temp/" . $newfilename;
// die($newfilename);

move_uploaded_file($_FILES["file"]["tmp_name"], $path);
// die("moved to $path");

$sourceCsvPath = $path;

// $sourceCsvPath = "FY 2014-2015.csv";

if (file_exists($sourceCsvPath)) {
	echo "<p>Target file $sourceCsvPath exists... proceeding.</p>";
} else {
	echo "<p>Target file $sourceCsvPath not found... aborting.</p>";
	die();
}

if (($handle = fopen($sourceCsvPath, "r")) !== FALSE) {
	// init data array
	$dataArray = array();

	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		// expect 14 fields in this report. 15 means those are the data headers
		$numFields = count($data);
		if ($numFields == 15) {
			// echo var_dump($data);
			// Generate headers
			$headers = array();
			foreach($data as $key => $header) {
				if ($header != "") {
					$headers[] = $header;
				}
			}

			$headerList = implode(", ", $headers);

			echo "<p>Headers set: $headerList</p>";
		} else if ($numFields == 14) {
			// init temporary array
			$toAdd = array();

			// parse and add to temp
			foreach ($data as $key => $value) {
				$targetHeader = $headers[$key];
				$toAdd[$targetHeader] = $value;
			}

			// add temp to final array
			$dataArray[] = $toAdd;
			// echo "<p>Added a row.</p>";
		}
	}

	$rowCount = count($dataArray);

	echo "<p>Parsing complete - added $rowCount rows to array.</p>";

	if (in_array("original", $_POST["expand"])) {
		echo "<p>Raw data:</p><pre>";
		print_r($dataArray);
		echo " </pre>";
	}

	echo "<h1>Attendee List</h1>";
	// init list
	$uniqueAttendees = array();
	$rawAttendeeCount = 0;

	foreach ($dataArray as $key => $value) {
		$attendee = $value["Attendee"];
		$rawAttendeeCount++;

		if (!in_array($attendee, $uniqueAttendees)) {
			$uniqueAttendees[] = $attendee;
		}
	}

	if (in_array("all-attendees", $_POST["expand"])) {
		echo "<p>Number of total attendees (before elimination of duplicates): $rawAttendeeCount</p>";
	}

	// account for duplicates with number in name

	$position = 0;
	$max = count($uniqueAttendees) - 1;

	// sort($uniqueAttendees);
	natcasesort($uniqueAttendees);
	$uniqueAttendees = array_values($uniqueAttendees); // reset keys

	while ($position < $max) {
		$next = $position + 1;

		$src = $uniqueAttendees[$position];
		$comp = $uniqueAttendees[$next];

		// echo "<p>Comparing $src to $comp...</p>";

		// let's get punctuation marks (except underscores) out... SCRAP FOR NOW
		// $src = preg_replace('/\W+ /', "", $src);
		// $comp = preg_replace('/\W+ /', "", $comp);

		$srcChunks = explode(" ", $src);
		$compChunks = explode(" ", $comp);

		// print_r($srcChunks);
		// print_r($compChunks);
		// die();

		$compLastChunk = array_pop($compChunks);
		$eliminate = false;
		$reason = "";
		if ($srcChunks == $compChunks && is_numeric($compLastChunk)) {
			// last bit is a number, and it matches otherwise - get rid of it!
			$eliminate = true;
			$reason = "numeric appendix detected";
		} else if ($src == $comp && count($srcChunks) > 1) {
			// direct match of another string - at least one space (we can't tell for sure that Bob and Bob are the same)
			$eliminate = true;
			$reason = "exact match of name with at least one space";
		} else if (strpos($comp, $src) !== FALSE && count($srcChunks) > 1) {
			// reasonable match of another string with at least one space (again avoiding Bob and Bob), targeting things like Bob Smith vs. Bob Smith, USA
			$eliminate = true;
			$reason = "reasonable match detected";
		} else if (strpos(strtolower($comp), strtolower($src)) !== FALSE && count($srcChunks) > 1) {
			// reasonable match, case-insensitive (handling separately for reporting)
			$eliminate = true;
			$reason = "case-insensitive reasonable match";
		} else if (is_numeric($compLastChunk)) {
			// match a name like Bob Smith 2 to Bob Smith, USA
			$newComp = implode(" ", $compChunks);
			if (strpos($src, $newComp) !== FALSE && count($compChunks) > 1) {
				// order is indeed reversed here, on purpose.
				$eliminate = true;
				$reason = "reasonable match (complex) detected";
			}
		} else if (percentDifference($src, $comp) <= 10 && count($srcChunks) > 1 && strlen($src) >= 9) {
			// using Levenshtein distance to catch spelling errors - 10% of the string or less matched, then it's probably a duplicate.
			// also must be 2 words, and at least 9 total characters
			// ignore if one of the string chunks is one letter (first or last initial)
			$ignore = false;

			foreach($srcChunks as $chunk) {
				if (strlen($chunk) == 1) {
					$ignore = true;
				}
			}
			foreach($compChunks as $chunk) {
				if (strlen($chunk) == 1) {
					$ignore = true;
				}
			}
			if (!$ignore) {
				$eliminate = true;
				$reason = "probable misspelling ($src vs $comp)";
			}
		}

		if ($eliminate) {
			if (in_array("eliminated", $_POST["expand"])) {
				echo "<p>ELIMINATING duplicate attendee: {$uniqueAttendees[$next]} - reason: $reason</p>";
			}
			unset($uniqueAttendees[$next]);
			$uniqueAttendees = array_values($uniqueAttendees);
			$max = count($uniqueAttendees) - 1;
			// do NOT increment...need to check same one again.
		} else {
			// move along...
			$position++;
		}
	}

	echo "<p>Unique attendees: " . count($uniqueAttendees) . "</p>";
	if (in_array("attendees", $_POST["expand"])) {
		echo "<p>Raw attendee list:</p>";
		echo "<ul>";
		foreach ($uniqueAttendees as $a) {
			echo "<li>$a</li>";
		}
		echo "</ul>";
	}


	echo "<h1>Room List</h1>";
	// rooms are unique if they have BOTH a different name AND a different start time.

	// init list
	$uniqueRooms = array();
	// init length statistic arrays
	$roomLengthData = array();
	$roomLengthRawData = array();

	foreach ($dataArray as $key => $value) {
		// check for name first
		$roomName = $value["Name"];
		$roomStart = formatDate($value["Starts"]);
		$roomEnd = formatDate($value["Ends"]);
		$roomLength = getLength($value["Starts"], $value["Ends"]);

		$found = false;
		foreach($uniqueRooms as $k => $v) {
			if ($roomName == $v["Name"] && $roomStart == $v["Starts"]) {
				$found = true;
			}
		}
		if (!$found) {
			// eliminate sessions that are short (probably test sessions)
			$thresholdInMinutes = 0; // foregoing this for more detailed metrics

			if ($roomLength >= $thresholdInMinutes) {
				$uniqueRooms[] = array(
					"Name" => $roomName,
					"Starts" => $roomStart,
					"Ends" => $roomEnd,
					"Length" => $roomLength
				);

				// add to counting arrays

				$roomLengthRawData[] = $roomLength;

				if (array_key_exists($roomLength, $roomLengthData)) {
					$roomLengthData[$roomLength]++;
				} else {
					$roomLengthData[$roomLength] = 1;
				}

			}
		}

	}

	// die(print_r($uniqueRooms));

	sort($uniqueRooms);
	ksort($roomLengthData);

	if ($thresholdInMinutes > 0) {echo "<p><em>Current room threshold is set to $thresholdInMinutes minutes - rooms active for less than this time are not counted.</em></p>";}

	echo "<p>Unique rooms: " . count($uniqueRooms) . "</p>";

	if (in_array("rooms", $_POST["expand"])) {
		echo "<p>Raw room list:</p>";
		echo "<ul>";
		foreach ($uniqueRooms as $r) {
			echo "<li>{$r["Name"]} (start time: {$r["Starts"]}; end time: {$r["Ends"]}; length: {$r["Length"]} minutes)</li>";
		}
		echo "</ul>";
	}

	if (in_array("stats", $_POST["expand"])) {
		echo "<h1>Room Length Statistics</h1>";
		// echo "<pre>";
		// print_r($roomLengthData);
		// echo "</pre>";

		// now for some basic analysis:
		$totalRooms = array_sum($roomLengthData);
		echo "<p>$totalRooms total Rooms</p>";

		echo roomsBetweenLength(0, 5);
		echo roomsBetweenLength(6, 15);
		echo roomsBetweenLength(16, 30);
		echo roomsBetweenLength(31, 60);
		echo "<br />";

		$cushion = 15;
		$marks = array(60, 120, 180);

		foreach($marks as $m) {
			echo roomsBetweenLength($m - $cushion, $m + $cushion);
		}

		//echo roomsBetweenLength(180);

		echo "<div id=\"chartdiv\" style=\"height:600px; width:600px\"></div>";

		$roomLengthDataFormatted = array();
		foreach($roomLengthData as $k => $v) {
			$roomLengthDataFormatted[] = array($k, $v);
		}
	}
}

fclose($handle);
unlink($sourceCsvPath); // delete from server after report runs.


} else { // end isset()
?>

<h1>Collaborate CSV Cruncher</h1>
<p><em>Note: use the <strong>Meeting Information Report</strong>, with Show By set to "Room."</em></p>

<form action="index.php" method="post" enctype="multipart/form-data">
	<label for="file">Upload Collaborate CSV Report (Meeting Information Report): </label>
	<input type="file" id="file" name="file"><br /><br />

	<label for="expand">Fields to expand:</label><br />
	<input type="checkbox" name="expand[]" id="expand" value="original">Original data array<br />
	<input type="checkbox" name="expand[]" id="expand" value="all-attendees" checked>Count of all attendees (before duplicate detection)<br />
	<input type="checkbox" name="expand[]" id="expand" value="spelling">Spell Checker log (&lt;= <?=$spellCheckerWarningPct?>% differences)<br />
	<input type="checkbox" name="expand[]" id="expand" value="eliminated">Duplicate attendees eliminated<br />
	<input type="checkbox" name="expand[]" id="expand" value="attendees">Attendees list<br />
	<input type="checkbox" name="expand[]" id="expand" value="rooms">Rooms list<br />
	<input type="checkbox" name="expand[]" id="expand" value="stats" checked>Room length statistics<br /><br />

	<input type="submit" name="submit" value="submit">
</form>

<?php
} // end else

if (isset($_POST["submit"])) {
	?>
	<script>
	$(function() {
		// $.jqplot('chartdiv', <?=json_encode($roomLengthDataFormatted)?>);
	});
	</script>
	<?php
}

?>

	</body>
</html>