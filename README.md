## BB COLLABORATE REPORT GENERATOR

### WHAT IS THIS?

    * Small script that ingests a Collaborate usage report and generates usage statistics used for reporting at UNCG.
    * Version history is at the base of this README file

### WHO MAINTAINS THIS?

    * UNCG LMS Administrators, who are part of the Application Administration group within Information Technology Services

### QUESTIONS? COMMENTS? CONCERNS?

    * 6-TECH@uncg.edu or lms-admins-l@uncg.edu


### VERSION HISTORY (most recent first)

    v1.0:
    - Initial release